﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace ERP
{
    namespace Encryption
    {
        public static class Encrypt
        {
            public static VFS.File CreateEncryptedFile(string FilePath, string EncryptionKey)
            {
                return CreateEncryptedFile(FilePath, EncryptionKey, false);
            }
            public static VFS.File CreateEncryptedFile(string FilePath, string EncryptionKey, bool isCompressed)
            {
                FileStream currentfile = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                MemoryStream currentdatastm = new MemoryStream();
                currentfile.CopyTo(currentdatastm);
                string filedata = Convert.ToBase64String(currentdatastm.ToArray());
                filedata = EncryptString(filedata, EncryptionKey);
                if (isCompressed == true) filedata = Compression.Compress.CompressString(filedata);
                return new VFS.File(FilePath, filedata, isCompressed);
            }
            private static string EncryptString(string EncryptString, string EncryptKey)
            {
                if (EncryptKey.Length < 8) throw new Exception("Encryption Key must be atleast 8 characters long.");
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
                try
                {
                    byte[] bykey = Encoding.UTF8.GetBytes(EncryptKey.Substring(0, 8));
                    byte[] InputByteArray = Encoding.UTF8.GetBytes(EncryptString);
                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    MemoryStream ms = new MemoryStream();
                    CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write);
                    cs.Write(InputByteArray, 0, InputByteArray.Length);
                    cs.FlushFinalBlock();
                    return Convert.ToBase64String(ms.ToArray());
                }
                catch (Exception e)
                {
                    throw new Exception("Error during encryption/decryption. Message: " + e.Message);
                }
            }
        }
        public static class Decrypt
        {
            public static MemoryStream DecryptFileData(VFS.File EncryptedFile, string DecryptKey)
            {
                if (EncryptedFile.Encrypted == false) throw new Exception("File is not Encrypted. Please attempt to read this file without decrypting it.");
                MemoryStream filedata = new MemoryStream(Convert.FromBase64String(DecryptString(EncryptedFile.FileDataString, DecryptKey)));
                return filedata;
            }
            private static string DecryptString(string EncryptedString, string DecryptKey)
            {
                if (DecryptKey.Length < 8) throw new Exception("Encryption Key must be atleast 8 characters long.");
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };
                try
                {
                    byte[] bykey = Encoding.UTF8.GetBytes(DecryptKey.Substring(0, 8));
                    byte[] InputByteArray = Convert.FromBase64String(EncryptedString);
                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    MemoryStream ms = new MemoryStream();
                    CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(bykey, IV), CryptoStreamMode.Write);
                    cs.Write(InputByteArray, 0, InputByteArray.Length);
                    cs.FlushFinalBlock();
                    return Encoding.UTF8.GetString(ms.ToArray());
                }
                catch (Exception e)
                {
                    throw new Exception("Error during encryption//decryption. Message: " + e.Message);
                }
            }
        }
    }
}
