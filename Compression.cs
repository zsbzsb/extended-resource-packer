﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace ERP
{
    internal static class Compression
    {
        internal static class Compress
        {
            internal static string CompressString(string DeCompressedString)
            {
                MemoryStream memstream = new MemoryStream();
                GZipStream compresser = new GZipStream(memstream, CompressionMode.Compress);
                byte[] stringbytes = Encoding.Unicode.GetBytes(DeCompressedString);
                compresser.Write(stringbytes, 0, stringbytes.Length);
                compresser.Close();
                return Convert.ToBase64String(memstream.ToArray());
            }
        }
        internal static class DeCompress
        {
            internal static string DeCompressString(string CompressedString)
            {
                MemoryStream memstreamin = new MemoryStream(Convert.FromBase64String(CompressedString));
                MemoryStream memstreamout = new MemoryStream();
                GZipStream decompressor = new GZipStream(memstreamin, CompressionMode.Decompress);
                byte[] outbytes = new byte[4096];
                int n = 0;
                while ((n = decompressor.Read(outbytes, 0, outbytes.Length)) != 0)
                {
                    memstreamout.Write(outbytes, 0, n);
                }
                decompressor.Close();
                memstreamout.Close();
                return Encoding.Unicode.GetString(memstreamout.ToArray());
            }
        }
    }
}
