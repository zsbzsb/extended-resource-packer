﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using ERP.VFS;

namespace ERP
{
    namespace FileControl
    {
        public static class VFSLoader
        {
            public static PackedFile LoadPackedFile(string FilePath)
            {
                PackedFile loadedfile = null;
                FileStream loadedfilestream = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                BinaryFormatter bf = new BinaryFormatter();
                loadedfile = (PackedFile)bf.Deserialize(loadedfilestream);
                loadedfilestream.Close();
                return loadedfile;
            }
        }
        public static class VFSWriter
        {
            public static void SavePackedFile(string FilePath, PackedFile CurrentPackedFile)
            {
                FileStream savefilestream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(savefilestream, CurrentPackedFile);
                savefilestream.Close();
            }
        }
    }
}
