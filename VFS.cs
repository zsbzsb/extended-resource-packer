﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace ERP
{
    namespace VFS
    {
        [Serializable]
        public class PackedFile
        {
            public Directory RootDirectory
            {
                get
                {
                    return _rootdirectory;
                }
            }
            private Directory _rootdirectory;
            public PackedFile()
            {
                _rootdirectory = new Directory("RootDirectory", true);
            }
            public PackedFile(PackedFile PackedFileCopy)
            {
                _rootdirectory = PackedFileCopy.RootDirectory;
            }
            public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            {
                info.AddValue("rootdirectory", _rootdirectory);
            }
            protected PackedFile(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            {
                info.GetValue("rootdirectory", _rootdirectory.GetType());
            }
            public Directory LocateDirectory(string DirectoryPath)
            {
                string[] pathlist = DirectoryPath.Split('\\');
                Directory looplist = RootDirectory;
                int c = 0;
                while (c < pathlist.Length)
                {
                    if (pathlist[c] == "")
                    {
                        c += 1;
                    }
                    else
                    {
                        int dc = 0;
                        while (dc < looplist.DirectoryList.Count)
                        {
                            if (looplist.DirectoryList[dc].DirectoryName.ToLowerInvariant() == pathlist[c].ToLowerInvariant())
                            {
                                if (c < (pathlist.Length - 1))
                                {
                                    looplist = looplist.DirectoryList[dc];
                                    break;
                                }
                                else
                                {
                                    return looplist.DirectoryList[dc];
                                }
                            }
                            dc += 1;
                        }
                        c += 1;
                    }
                }
                throw new Exception("Unable to locate specified Directory.");
            }
            public Directory LocateParentDirectory(Directory CurrentDirectory)
            {
                return LocateParentDirectory(CurrentDirectory.VirtualDirectoryPath);
            }
            public Directory LocateParentDirectory(string DirectoryPath)
            {
                string[] pathlist = DirectoryPath.Split('\\');
                Directory looplist = RootDirectory;
                int c = 0;
                while (c < pathlist.Length)
                {
                    if (pathlist[c] == "")
                    {
                        c += 1;
                    }
                    else
                    {
                        int dc = 0;
                        while (dc < looplist.DirectoryList.Count)
                        {
                            if (looplist.DirectoryList[dc].DirectoryName.ToLowerInvariant() == pathlist[c].ToLowerInvariant())
                            {
                                if (c < (pathlist.Length - 1))
                                {
                                    looplist = looplist.DirectoryList[dc];
                                    break;
                                }
                                else
                                {
                                    return looplist;
                                }
                            }
                            dc += 1;
                        }
                        c += 1;
                    }
                }
                throw new Exception("Unable to locate specified Directory.");
            }
            public File LocateFile(string FilePath)
            {
                string[] pathlist = FilePath.Split('\\');
                Directory looplist = RootDirectory;
                int c = 0;
                while (c < pathlist.Length)
                {
                    if (pathlist[c] == "")
                    {
                        c += 1;
                    }
                    else
                    {
                        if (c < (pathlist.Length - 1))
                        {
                            int dc = 0;
                            while (dc < looplist.DirectoryList.Count)
                            {
                                if (looplist.DirectoryList[dc].DirectoryName.ToLowerInvariant() == pathlist[c].ToLowerInvariant())
                                {
                                    looplist = looplist.DirectoryList[dc];
                                    break;
                                }
                                dc += 1;
                            }
                        }
                        else
                        {
                            int fc = 0;
                            while (fc < looplist.FileList.Count)
                            {
                                if (looplist.FileList[fc].FileName.ToLowerInvariant() == pathlist[c].ToLowerInvariant())
                                {
                                    return looplist.FileList[fc];
                                }
                                fc += 1;
                            }
                        }
                        c += 1;
                    }
                }
                throw new Exception("Unable to locate specified File.");
            }
            public void AddFile(File NewFile)
            {
                _rootdirectory.AddFile(NewFile);
            }
            public void AddDirectory(Directory NewDirectory)
            {
                _rootdirectory.AddDirectory(NewDirectory);
            }
            public List<File> GetFileList()
            {
                List<File> filelist = new List<File>();
                _rootdirectory.AddFilestoList(filelist);
                return filelist;
            }
            public List<Directory> GetDirectoryList()
            {
                List<Directory> directorylist = new List<Directory>();
                _rootdirectory.AddDirectoriestoList(directorylist);
                return directorylist;
            }
        }
        [Serializable]
        public class Directory : ISerializable
        {
            public List<Directory> DirectoryList
            {
                get
                {
                    return _directorylist;
                }
            }
            public List<File> FileList
            {
                get
                {
                    return _filelist;
                }
            }
            public string DirectoryName
            {
                get
                {
                    return _directoryname;
                }
            }
            public string VirtualDirectoryPath
            {
                get
                {
                    return _vdirectorypath;
                }
                internal set
                {
                    _vdirectorypath = value;
                    int c = 0;
                    while (c < _directorylist.Count)
                    {
                        _directorylist[c].VirtualDirectoryPath = _vdirectorypath + "\\" + _directorylist[c].DirectoryName;
                        c += 1;
                    }
                    c = 0;
                    while (c < _filelist.Count)
                    {
                        _filelist[c].VirtualFilePath = _vdirectorypath + "\\" + _filelist[c].FileName;
                        c += 1;
                    }
                }
            }
            private List<Directory> _directorylist = new List<Directory>();
            private List<File> _filelist = new List<File>();
            private string _directoryname = "";
            private string _vdirectorypath = "";
            public Directory(string NewDirectoryName)
            {
                _directorylist = new List<Directory>();
                _filelist = new List<File>();
                _directoryname = NewDirectoryName;
                VirtualDirectoryPath = ("\\" + NewDirectoryName);
            }
            internal Directory(string NewDirectoryName, bool SkipPath)
            {
                _directorylist = new List<Directory>();
                _filelist = new List<File>();
                _directoryname = NewDirectoryName;
                if (SkipPath == false) VirtualDirectoryPath = ("\\" + NewDirectoryName);
            }
            //public Directory(Directory DirectoryCopy)
            //{
            //    _directorylist = new List<Directory>();
            //    foreach (Directory dr in DirectoryCopy._directorylist) { _directorylist.Add(dr); }
            //    _filelist = new List<File>();
            //    foreach (File fl in DirectoryCopy._filelist) { _filelist.Add(fl); }
            //    _directoryname = DirectoryCopy._directoryname;
            //    VirtualDirectoryPath = "\\" + DirectoryCopy.DirectoryName;
            //}
            public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            {
                info.AddValue("directorylist", _directorylist);
                info.AddValue("filelist", _filelist);
                info.AddValue("directoryname", _directoryname);
                info.AddValue("vdirectorypath", _vdirectorypath);
            }
            protected Directory(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            {
                _directorylist = (List<Directory>)info.GetValue("directorylist", _directorylist.GetType());
                _filelist = (List<File>)info.GetValue("filelist", _filelist.GetType());
                _directoryname = info.GetString("directoryname");
                _vdirectorypath = info.GetString("vdirectorypath");
            }
            public void AddFile(File NewFile)
            {
                foreach (File fl in _filelist) { if (fl.FileName == NewFile.FileName) throw new Exception("File must have a unique name."); }
                NewFile.VirtualFilePath = _vdirectorypath + "\\" + NewFile.FileName;
                _filelist.Add(NewFile);
            }
            public void RemoveFile(File CurrentFile)
            {
                if (_filelist.Contains(CurrentFile)) _filelist.Remove(CurrentFile);
            }
            public void AddDirectory(Directory NewDirectory)
            {
                foreach (Directory dr in _directorylist) { if (dr.DirectoryName == NewDirectory.DirectoryName) throw new Exception("Directory must have a unique name."); }
                NewDirectory.VirtualDirectoryPath = _vdirectorypath + "\\" + NewDirectory.DirectoryName;
                _directorylist.Add(NewDirectory);
            }
            public void RemoveDirectory(Directory CurrentDirectory)
            {
                if (_directorylist.Contains(CurrentDirectory)) _directorylist.Remove(CurrentDirectory);
            }
            internal void AddFilestoList(List<File> CurrentFileList)
            {
                int c = 0;
                while (c < _filelist.Count)
                {
                    CurrentFileList.Add(_filelist[c]);
                    c += 1;
                }
                c = 0;
                while (c < _directorylist.Count)
                {
                    _directorylist[c].AddFilestoList(CurrentFileList);
                    c += 1;
                }
            }
            internal void AddDirectoriestoList(List<Directory> CurrentDirectoryList)
            {
                int c = 0;
                while (c < _directorylist.Count)
                {
                    CurrentDirectoryList.Add(_directorylist[c]);
                    _directorylist[c].AddDirectoriestoList(CurrentDirectoryList);
                    c += 1;
                }
            }
        }
        [Serializable]
        public class File : ISerializable
        {
            public string FileName
            {
                get
                {
                    return _filename;
                }
            }
            public MemoryStream FileData
            {
                get
                {
                    if (_encrypted == true) throw new Exception("File is encrypted. Please Decrypt first using the Encryption Namespace");
                    else
                    {
                        if (_compressed == true) return new MemoryStream(Convert.FromBase64String(Compression.DeCompress.DeCompressString(_filedata)));
                        else return new MemoryStream(Convert.FromBase64String(_filedata));
                    } 
                }
            }
            internal string FileDataString
            {
                get
                {
                    if (_compressed == true) return Compression.DeCompress.DeCompressString(_filedata);
                    else return _filedata;
                }
            }            
            public string VirtualFilePath
            {
                get
                {
                    return _vfilepath;
                }
                internal set
                {
                    _vfilepath = value;
                }
            }
            public bool Encrypted
            {
                get
                {
                    return _encrypted;
                }
            }
            public bool Compressed
            {
                get
                {
                    return _compressed;
                }
            }
            private string _filename = "";
            private string _filedata = "";
            private string _vfilepath = "";
            private bool _encrypted = false;
            private bool _compressed = false;
            internal File(string FilePath, string EncryptedFileData, bool isCompressed)
            {
                _filename = Path.GetFileName(FilePath);
                _filedata = EncryptedFileData;
                _encrypted = true;
                _compressed = isCompressed;
            }
            public File(string FilePath, bool isCompressed)
            {
                _filename = Path.GetFileName(FilePath);
                FileStream currentfile = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                MemoryStream currentdatastm = new MemoryStream();
                currentfile.CopyTo(currentdatastm);
                _filedata = Convert.ToBase64String(currentdatastm.ToArray());
                if (isCompressed == true)
                {
                    _filedata = Compression.Compress.CompressString(_filedata);
                    _compressed = true;
                }
                currentfile.Close();
                currentdatastm.Close();
            }
            public File(string FilePath)
            {
                _filename = Path.GetFileName(FilePath);
                FileStream currentfile = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                MemoryStream currentdatastm = new MemoryStream();
                currentfile.CopyTo(currentdatastm);
                _filedata = Convert.ToBase64String(currentdatastm.ToArray());
                currentfile.Close();
                currentdatastm.Close();
            }
            public File(File FileCopy)
            {
                _filename = FileCopy._filename;
                _filedata = FileCopy._filedata;
                _vfilepath = FileCopy._vfilepath;
                _encrypted = FileCopy._encrypted;
                _compressed = FileCopy._compressed;
            }
            public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            {
                info.AddValue("filename", _filename);
                info.AddValue("filedata", _filedata);
                info.AddValue("vfilepath", _vfilepath);
                info.AddValue("encrypted", _encrypted);
                info.AddValue("compressed", _compressed);
            }
            protected File(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            {
                _filename = info.GetString("filename");
                _filedata = info.GetString("filedata");
                _vfilepath = info.GetString("vfilepath");
                _encrypted = info.GetBoolean("encrypted");
                _compressed = info.GetBoolean("compressed");
            }
        }
    }
}
